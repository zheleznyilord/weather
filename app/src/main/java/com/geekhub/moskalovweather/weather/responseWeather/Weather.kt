package com.geekhub.moskalovweather.weather.responseWeather

import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class Weather(val city: City,
                   val coord: Coordinates,
                   val country: String,
                   val timezone: Int,
                   val cod: Int,
                   val message: Float,
                   val cnt: Int,
                   val list: List<Day>)

