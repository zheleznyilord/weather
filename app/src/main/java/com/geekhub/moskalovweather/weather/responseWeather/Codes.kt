package com.geekhub.moskalovweather.weather.responseWeather

import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class Codes(val id: Int,
                 val main: String,
                 val descriptor: String,
                 val icon: String)