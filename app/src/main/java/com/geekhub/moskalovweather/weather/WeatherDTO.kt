package com.geekhub.moskalovweather.weather

data class WeatherDTO(
    val day: String,
    val status: String,
    val dayOfWeek: String,
    var temperature: Float,
    var count:Int
)