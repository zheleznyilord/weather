package com.geekhub.moskalovweather.weather.responseWeather

import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class Day(val dt: Long,
               val main: Temperature,
               val weather: List<Codes>,
               val clouds: Cloud,
               val wind: Wind,
               val sys: Rain,
               val dt_txt: String)
