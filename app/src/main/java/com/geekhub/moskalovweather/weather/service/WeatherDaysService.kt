package com.geekhub.moskalovweather.weather.service

import com.geekhub.moskalovweather.weather.WeatherDTO
import com.geekhub.moskalovweather.weather.responseWeather.Weather
import java.text.SimpleDateFormat
import java.util.*


class WeatherDaysService(var weather: Weather) {
    var days = mutableMapOf<String, WeatherDTO>()

    init {
        for (day in weather.list) {
            val date = day.dt_txt.substring(0, 10)
            if (!days.containsKey(date)) {
                val newDate = if (date.substring(8).startsWith("0")) {
                    Date(date.substring(0, 4).toInt(), date.substring(5, 7).toInt(), date.substring(9).toInt())
                } else {
                    Date(date.substring(0, 4).toInt(), date.substring(5, 7).toInt(), date.substring(8).toInt())
                }
                var simpleDateFormat = SimpleDateFormat("EEEE")
                var format = simpleDateFormat.format(newDate)
                for (codes in day.weather) {
                    days[date] = WeatherDTO(date, codes.main,format,day.main.temp, 1)
1                }
            } else {
                var weatherDTO = days[date]
                if (weatherDTO != null) {
                    weatherDTO.temperature = weatherDTO.temperature + day.main.temp
                    weatherDTO.count++
                    days[date] = weatherDTO
                }
            }
        }
    }


}