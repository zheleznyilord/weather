package com.geekhub.moskalovweather.weather.service

import com.geekhub.moskalovweather.weather.responseWeather.Weather
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface WeatherService {
    @GET("forecast")
    fun getCurrentWeather(@Query("id") id: Int, @Query("APPID") appid: String): Call<Weather>
}
