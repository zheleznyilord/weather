package com.geekhub.moskalovweather.weather.responseWeather

import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class Rain(val pod: String)
