package com.geekhub.moskalovweather.database

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import android.provider.BaseColumns

class DBHelper(context: Context) : SQLiteOpenHelper(context,
    DATABASE_NAME, null, DATABASE_VERSION){


    override fun onCreate(db: SQLiteDatabase) {
        db.execSQL(SQL_CREATE_ENTRIES)
    }

    override fun onUpgrade(db: SQLiteDatabase, oldVersion: Int, newVersion: Int) {
        db.execSQL(SQL_DELETE_ENTRIES)
        onCreate(db)
    }

    override fun onDowngrade(db: SQLiteDatabase, oldVersion: Int, newVersion: Int) {
        onUpgrade(db, oldVersion, newVersion)
    }
    companion object {
        const val DATABASE_VERSION = 1
        const val DATABASE_NAME = "FeedReader.db"

        // Table contents are grouped together in an anonymous object.
        object FeedEntry : BaseColumns {
            const val TABLE_NAME = "weather"
            const val DAY = "day"
            const val TEMPERATURE = "temperature"
            const val STATUS ="status"
            const val DAY_OF_WEEK = "week"
            const val COUNT = "count"
        }

        private const val SQL_CREATE_ENTRIES =
            "CREATE TABLE IF NOT EXISTS ${FeedEntry.TABLE_NAME} (" +
                    "${BaseColumns._ID} INTEGER PRIMARY KEY," +
                    "${FeedEntry.DAY} DATE," +
                    "${FeedEntry.TEMPERATURE} FLOAT," +
                    "${FeedEntry.STATUS} TEXT," +
                    "${FeedEntry.DAY_OF_WEEK} TEXT," +
                    "${FeedEntry.COUNT} INTEGER)"
        private const val SQL_DELETE_ENTRIES
                = "DROP TABLE IF EXISTS ${FeedEntry.TABLE_NAME}"
    }


}