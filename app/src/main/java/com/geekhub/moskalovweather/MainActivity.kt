package com.geekhub.moskalovweather

import android.content.ContentValues
import android.content.res.Configuration
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.FragmentTransaction
import com.geekhub.moskalovweather.database.DBHelper
import com.geekhub.moskalovweather.weather.WeatherDTO
import com.geekhub.moskalovweather.weather.responseWeather.Weather
import com.geekhub.moskalovweather.weather.service.WeatherService
import com.geekhub.moskalovweather.weather.service.WeatherDaysService
import kotlinx.android.synthetic.main.activity_main.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory


class MainActivity : AppCompatActivity() {
    private val adapter by lazy { MainRecyclerAdapter() }
    lateinit var service: WeatherDaysService
    private lateinit var fragment: MainFragment
    private lateinit var fTrans: FragmentTransaction


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        fragment = MainFragment()

        if (resources.configuration.orientation.equals(Configuration.ORIENTATION_LANDSCAPE)) {
            fTrans = supportFragmentManager.beginTransaction()
            fTrans.add(R.id.frgm, fragment)
            fTrans.commit()
        }

        recycler_view.adapter = adapter

        var dbHelper = DBHelper(this)

        val writableDatabase = dbHelper.writableDatabase

        val retrofit = Retrofit.Builder()
            .baseUrl("http://api.openweathermap.org/data/2.5/")
            .addConverterFactory(GsonConverterFactory.create())
            .build()

        val weatherService: WeatherService = retrofit.create(WeatherService::class.java)

        val currentWeather = weatherService.getCurrentWeather(710791, "68dcfcfc5c3ca87ddf144cec9e1b86b2")

        currentWeather.enqueue(object : Callback<Weather> {
            override fun onFailure(call: Call<Weather>, t: Throwable) {
                println()
            }

            override fun onResponse(call: Call<Weather>, response: Response<Weather>) {
                service = response.body()?.let { WeatherDaysService(it) }!!
                var weathers = service.days.values.toList()
                writableDatabase.delete(DBHelper.DATABASE_NAME,null,null)

                for (value in weathers) {
                    val values = ContentValues().apply {
                        put(DBHelper.Companion.FeedEntry.DAY, value.day)
                        put(DBHelper.Companion.FeedEntry.TEMPERATURE, value.temperature)
                        put(DBHelper.Companion.FeedEntry.COUNT, value.count)
                        put(DBHelper.Companion.FeedEntry.DAY_OF_WEEK, value.dayOfWeek)
                        put(DBHelper.Companion.FeedEntry.STATUS, value.status)
                    }

                    val newRowId = writableDatabase.insert(DBHelper.Companion.FeedEntry.TABLE_NAME, null, values)

                }
                //response.body()?.let { it.list.forEach{ adapter.addWeather(it)} }
            }

        })

        val cursor = writableDatabase.query(DBHelper.Companion.FeedEntry.TABLE_NAME,null,
            null,null,null,null,null)
        val items = mutableListOf<WeatherDTO>()
        while (cursor.moveToNext()){
            val day = cursor.getString(cursor.getColumnIndexOrThrow(DBHelper.Companion.FeedEntry.DAY))
            val week = cursor.getString(cursor.getColumnIndexOrThrow(DBHelper.Companion.FeedEntry.DAY_OF_WEEK))
            val status = cursor.getString(cursor.getColumnIndexOrThrow(DBHelper.Companion.FeedEntry.STATUS))
            val temperature = cursor.getFloat(cursor.getColumnIndexOrThrow(DBHelper.Companion.FeedEntry.TEMPERATURE))
            val count = cursor.getInt(cursor.getColumnIndexOrThrow(DBHelper.Companion.FeedEntry.COUNT))

            val weather = WeatherDTO(day,status,week,temperature,count)
            items.add(weather)
        }
        cursor.close()
        adapter.addWeathers(items)

    }
}
