package com.geekhub.moskalovweather

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.geekhub.moskalovweather.weather.WeatherDTO
import kotlinx.android.synthetic.main.item_main.view.*

class MainRecyclerAdapter : RecyclerView.Adapter<MainRecyclerAdapter.ViewHolder>() {
    private val temps = mutableListOf<WeatherDTO>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_main, parent, false)
        return ViewHolder(view)
    }

    fun addWeathers(days: List<WeatherDTO>){
        temps.addAll(days)
        notifyDataSetChanged()
    }

    fun addWeather(day: WeatherDTO) {
        temps.add(day)
        notifyDataSetChanged()
    }

    override fun getItemCount(): Int {
        return temps.count()
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.onBind(temps[position])
    }


    inner class ViewHolder(private val view: View) : RecyclerView.ViewHolder(view) {
        private lateinit var day: WeatherDTO


        fun onBind(temp: WeatherDTO) {
            view.day.text = temp.day + " " + temp.dayOfWeek
            view.temperature.text = temp.status
            view.count.text = (temp.temperature/temp.count).toString()
            when(temp.status){
                "Clouds" -> view.temp.setImageResource(R.drawable.clouds)
                "Clear" -> view.temp.setImageResource(R.drawable.clear)
                "Snow" -> view.temp.setImageResource(R.drawable.snow)
                else -> view.temp.setImageResource(R.drawable.rain)
            }


            day = temp
        }
    }
}
